<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(attributes={
 *      "normalization_context"={"groups"={"user.read"}},
 *      "denormalization_context"={"groups"={"user.write"}}
 * })
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({
     *      "user.read"
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *      "user.read",
     *      "user.write"
     * })
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *      "user.read",
     *      "user.write"
     * })
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *      "user.read",
     *      "user.write"
     * })
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=10)
     * @Groups({
     *      "user.read",
     *      "user.write"
     * })
     */
    private $mobile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Roles", inversedBy="users", cascade="persist")
     * @Groups({
     *      "user.read",
     *      "user.write"
     * })
     */
    private $roles;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getRoles(): ?Roles
    {
        return $this->roles;
    }

    public function setRoles(?Roles $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}
