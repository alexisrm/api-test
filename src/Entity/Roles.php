<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(attributes ={
 *      "normalization_context"={"groups"={"role.read"}},
 *      "denormalization_context"={"groups"={"role.write"}}
 * })
 * @ORM\Entity(repositoryClass="App\Repository\RolesRepository")
 */
class Roles
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({
     *      "role.read"
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *      "role.read",
     *      "role.write",
     * 
     *      "user.read",
     *      "user.write",
     * 
     *      "user.update"
     * })
     */
    private $LibelleRole;

    /**
     * @ORM\Column(type="integer")
     * @Groups({
     *      "role.read",
     *      "role.write",
     * 
     *      "user.read",
     *      "user.write",
     * 
     *      "user.update"
     * })
     */
    private $roleLevel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="roles")
     * @Groups({
     *      "role.read",
     *      "role.write"
     * })
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleRole(): ?string
    {
        return $this->LibelleRole;
    }

    public function setLibelleRole(string $LibelleRole): self
    {
        $this->LibelleRole = $LibelleRole;

        return $this;
    }

    public function getRoleLevel(): ?int
    {
        return $this->roleLevel;
    }

    public function setRoleLevel(int $roleLevel): self
    {
        $this->roleLevel = $roleLevel;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setRoles($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getRoles() === $this) {
                $user->setRoles(null);
            }
        }

        return $this;
    }
}
